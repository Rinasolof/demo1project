import { apiProvider } from '../framework';
import { Severity } from "jest-allure/dist/Reporter";

test('Provider demo', async () => {
    reporter
    .description("Feature should work cool")
    .severity(Severity.Critical)
    .feature('Feature.Betting')
    .story("BOND-007");
reporter.startStep("Готовим данные");
// expect that it's fancy
    const body = {
        username: 'demo',
        password: 'demo',
    };
reporter.endStep();
reporter.startStep("Отправляем запрос");
    const r = await apiProvider().login().post(body);
    reporter.endStep();
    expect(r.status).toBe(200);
});
